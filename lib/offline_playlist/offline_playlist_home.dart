import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:miniplayer/miniplayer.dart';
import 'package:just_audio/just_audio.dart';

import 'package:ytdl_flutter/offline_playlist/audio_source.dart';
import 'package:ytdl_flutter/offline_playlist/offline_playlist_theme.dart';
import 'package:ytdl_flutter/db/ytdl_dao.dart';
import 'package:ytdl_flutter/db/database.dart';

class OfflinePlaylistHomeScreen extends StatefulWidget {
  @override
  _OfflinePlaylistHomeScreenState createState() => _OfflinePlaylistHomeScreenState();
}
class _OfflinePlaylistHomeScreenState extends State<OfflinePlaylistHomeScreen>
    with TickerProviderStateMixin {
  AnimationController? animationController;
  List<YtdlDao> videoList = [];
  final ScrollController _scrollController = ScrollController();
  Uint8List selectedAudioFile=Uint8List(0);
  final AudioPlayer player = AudioPlayer();

  @override
  void initState()  {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    getPlaylistVideo();
    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 200));
    return true;
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: OfflinePaylistTheme.buildLightTheme(),
      child: Container(
        child: Scaffold(
          body: Stack(
            children: <Widget>[
              InkWell(
                splashColor: Colors.transparent,
                focusColor: Colors.transparent,
                highlightColor: Colors.transparent,
                hoverColor: Colors.transparent,
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: Column(
                  children: <Widget>[
                    getAppBarUI(),
                    Expanded(
                      child: NestedScrollView(
                        controller: _scrollController,
                        headerSliverBuilder:
                            (BuildContext context, bool innerBoxIsScrolled) {
                          return <Widget>[];
                        },
                        body: Container(
                          color:
                          OfflinePaylistTheme.buildLightTheme().backgroundColor,
                          child: ListView.builder(
                            itemCount: videoList.length,
                            padding: const EdgeInsets.only(top: 8),
                            scrollDirection: Axis.vertical,
                            itemBuilder: (BuildContext context, int index) {
                                return getItem(index);
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              getMusicPlayer(),
            ],
          ),
        ),
      ),
    );
  }

  Widget getAppBarUI() {
    return Container(
      decoration: BoxDecoration(
        color: OfflinePaylistTheme.buildLightTheme().backgroundColor,
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              offset: const Offset(0, 2),
              blurRadius: 8.0),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top, left: 8, right: 8),
        child: Row(
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              width: AppBar().preferredSize.height + 40,
              height: AppBar().preferredSize.height,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(32.0),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.arrow_back),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: Text(
                  'Offline Playlist',
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 22,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Slidable getItem(int index) {
    return Slidable(
      key: ValueKey(index),
      startActionPane: ActionPane(
        motion: const ScrollMotion(),
        dismissible: DismissiblePane(onDismissed: () async{
          await YtdlDb.deleteData(videoList[index].video_id);
        }),
        children:  [],
      ),

      child: Expanded(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.only(left: 16, top: 8, bottom: 8),
            child: Column(
              mainAxisAlignment:
              MainAxisAlignment.center,
              crossAxisAlignment:
              CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Image.memory(videoList[index].image_file,
                        width: 80,
                        // height: 80,
                        fit: BoxFit.contain,),
                      Expanded(
                        child : Text(
                          videoList[index].title,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 22,
                          ),
                        ),
                      ),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(32.0),
                          ),
                          onTap: ()=>{playMusic(videoList[index])},
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.play_circle ,
                              color: OfflinePaylistTheme.buildLightTheme()
                                  .primaryColor,
                            ),
                          ),
                        ),
                      ),
                    ]
                ),
                Row(
                  crossAxisAlignment:
                  CrossAxisAlignment.center,
                  mainAxisAlignment:
                  MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child :  Text(
                        videoList[index].author,
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.grey
                                .withOpacity(0.8)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Miniplayer getMusicPlayer() {
    return Miniplayer(
      minHeight: 70,
      maxHeight: 370,
      builder: (height, percentage) {
        return Center(
          child: Text('$height, $percentage'),
        );
      },
    );
  }

  getPlaylistVideo() async {
    // fetch from DB
    videoList = await YtdlDb.showAllData();
    print("????" + videoList.toString());
    setState(() {});
  }

  playMusic(YtdlDao video) async {
    print('>>>> : '+video.toString());
    // copy byte stream to local variable
    selectedAudioFile = await YtdlDb.GetAudioFile(video.video_id);
    await player.setAudioSource(MyCustomSource(selectedAudioFile));
    player.play();
    print(selectedAudioFile);
    // change state ?
    // start playing
  }
}
