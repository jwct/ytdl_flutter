import 'dart:async';
import 'dart:typed_data';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:ytdl_flutter/db/ytdl_dao.dart';

class YtdlDb {
  static  Database ?database ;

  // Initialize database
  static Future<Database?> initDatabase() async {
    YtdlDb.database = await openDatabase(
      // Ensure the path is correctly for any platform
      join(await getDatabasesPath(), "ytdl_database.db"),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE YTDL("
                "video_id TEXT PRIMARY KEY,"
                "title TEXT,"
                "author TEXT,"
                "audio_content_type TEXT,"
                "audio_file BLOB,"
                "image_file BLOB"
                ")"
        );
      },

      // Version
      version: 1,
    );

    return database;
  }

  // Check database connected
  static Future<Database?> getDatabaseConnect() async {
    if (database != null) {
      return database;
    }
    else {
      return await initDatabase();
    }
  }

  // Show all data
  static Future<List<YtdlDao>> showAllData() async {
    final db = await getDatabaseConnect();
    if (db != null) {
      final List<Map<String, dynamic>> maps = await db.query("YTDL");

      return List.generate(maps.length, (i) {
        return YtdlDao(
          video_id: maps[i]["video_id"],
          title: maps[i]["title"],
          author: maps[i]["author"],
          audio_content_type: maps[i]["audio_content_type"],
          image_file: maps[i]["image_file"],
        );
      });
    }else {
      return List.empty();
    }
  }

  // Insert
  static Future<void> insertData(YtdlDao ytdlDao) async {
    final db = await getDatabaseConnect();
    if ( db != null ) {
      await db.insert(
        "YTDL",
        ytdlDao.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace,
      );
    }
  }

  // Update
  static Future<void> updateData(YtdlDao ytdlDao) async {
    final db = await getDatabaseConnect();
    if ( db != null ) {
      await db.update(
        "YTDL",
        ytdlDao.toMap(),
        where: "video_id = ?",
        whereArgs: [ytdlDao.video_id],
      );
    }
  }

  // Update
  static Future<void> putAudioFile(String video_id , Uint8List audio_file) async {
    final db = await getDatabaseConnect();
    if ( db != null ) {
      await db.update(
        "YTDL",
        {"audio_file": audio_file,},
        where: "video_id = ?",
        whereArgs: [video_id],
      );
    }
  }
  // Get
  static Future<Uint8List> GetAudioFile(String video_id ) async {
    final db = await getDatabaseConnect();
    if ( db != null ) {
      var result = await db.query(
        "YTDL",
        columns: ["audio_file"],
        where: "video_id = ?",
        whereArgs: [video_id],
      );
      return (await result.first)['audio_file'] as Uint8List;
    }
    return Uint8List(0);
  }

  // Delete
  static Future<void> deleteData(String video_id) async {
    final db = await getDatabaseConnect();
    if ( db != null ) {
      await db.delete(
        "YTDL",
        where: "video_id = ?",
        whereArgs: [video_id],
      );
    }
  }
}