import 'dart:typed_data';

class YtdlDao {
  // Init
  final String video_id;
  final String title;
  final String author;
  final String audio_content_type;
  final Uint8List image_file;

  YtdlDao(
      {
        required this.video_id
        , required this.title
        , required this.author
        , required this.audio_content_type
        , required this.image_file
      });

  // toMap()
  Map<String, dynamic> toMap() {
    return {
      "video_id": video_id,
      "title": title,
      "author": author,
      "audio_content_type" : audio_content_type,
      "image_file": image_file,
    };
  }

  @override
  String toString() {
    return "YtdlDao{\n video_id: $video_id\n  title: $title\n author: $author \n audio_content_type:$audio_content_type \n}\n\n";
  }
}