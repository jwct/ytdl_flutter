import 'package:flutter/widgets.dart';

import 'package:ytdl_flutter/youtube_playlist/youtube_playlist_home.dart';
import 'package:ytdl_flutter/offline_playlist/offline_playlist_home.dart';

class HomeList {
  HomeList({
    this.navigateScreen,
    this.imagePath = '',
  });

  Widget? navigateScreen;
  String imagePath;

  static List<HomeList> homeList = [
    HomeList(
      imagePath: 'assets/youtube_pl/ytpl_home.png',
      navigateScreen: YoutubePlaylistHomeScreen(),
    ),
    HomeList(
      imagePath: 'assets/offline_pl/olpl_home.png',
      navigateScreen: OfflinePlaylistHomeScreen(),
    ),
  ];
}
