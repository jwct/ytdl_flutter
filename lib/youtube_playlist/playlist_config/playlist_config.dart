import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class PlaylistConfig {
   List<String> _playlistIds = [];

  /// private constructor
   PlaylistConfig._();
  /// the one and only instance of this singleton
  static final instance = PlaylistConfig._();

  static readConfig() async {
    await dotenv.load(fileName: "assets/.env");

    String channel_ids = dotenv.get('PLAYLIST_IDS');

    instance._playlistIds = json.decode(channel_ids)
        .map<String>((e) => e.toString() )
        .toList();

  }

  static List<String> getPlaylistIds() {
    return instance._playlistIds;
  }
}
