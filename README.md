# ytdl_flutter

YTDL flutter : a youtube downloader in flutter

## Usage 

1. Rename .env_sample to .env
2. Put your youtube playlist list in PLAYLIST_IDS. (The playlist should be public or unlisted, private playlist is not supported.)

```
PLAYLIST_IDS=["PL_aaaaabbbbbcccccdddddeeeeefffffg","PL_zzzzzyyyyyxxxxxwwwwwvvvvvuuuuut"]
```

3. Start your flutter app or deploy to your mobile device

## Credit

- UI template : [mitesh77/Best-Flutter-UI-Templates](https://github.com/mitesh77/Best-Flutter-UI-Templates/tree/master/best_flutter_ui_templates)

- Podcast icon image : [Image location](https://www.freeiconspng.com/downloadimg/28977)

- Offline playlist image : [Image location](https://www.freepik.com/free-vector/illustation-music-concpet_2825875.htm)

- Youtube playlist image : [Image location](https://www.freepik.com/free-vector/realistic-multimedia-player-template_4264556.htm)